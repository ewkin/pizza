import axios from 'axios';

export const axiosPizza = axios.create({
    baseURL: 'https://restaurant-menu-83e59-default-rtdb.firebaseio.com/pizza'
});
