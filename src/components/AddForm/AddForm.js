import React, {useState} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {createMenuItem, editMenuItem} from "../../store/actions/adminDishAction";

const AddForm = () => {
    const dispatch = useDispatch();
    const id = useSelector(state => state.menu.id);

    const [pizza, setPizza] = useState({
        name: '',
        price: '',
        url: '',
    });
    let button = 'Add';

    const customerDataChanged = event => {
        const {name, value} = event.target;

        setPizza(prevState => ({
            ...prevState,
            [name]: value
        }));
    };

    let pizzaHandler = event => {
        event.preventDefault();
        dispatch(createMenuItem(pizza));
    }
    if(id){
        button = 'Edit';
        pizzaHandler= event =>{
            event.preventDefault();
            dispatch(editMenuItem(pizza, id));
        }
    }
    return (
        <form onSubmit={pizzaHandler}>
            <div className="mb-3">
                <label htmlFor="name" className="form-label">Name</label>
                <input required type="text" className="form-control" id="name" name="name"
                       placeholder="Pizza name" value={pizza.name}
                       onChange={customerDataChanged}/>
            </div>
            <div className="mb-3">
                <label htmlFor="price" className="form-label">Price</label>
                <input required type="text" className="form-control" id="price" name="price"
                       placeholder="Pizza price" value={pizza.price}
                       onChange={customerDataChanged}/>
            </div>
            <div className="mb-3">
                <label htmlFor="url" className="form-label">Pizza picture</label>
                <input required type="text" className="form-control" id="url" name="url"
                       placeholder="Pizza picture" value={pizza.url}
                       onChange={customerDataChanged}/>
            </div>
            <button type='submit' className="btn btn-success">{button}</button>
        </form>
    );

};

export default AddForm;