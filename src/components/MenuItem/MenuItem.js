import React from 'react';

const MenuItem = ({name, price, url, editDish, removeDish}) => {

    return (
        <div className="col-sm-3">
            <div className="card">
                <img src={url} className="card-img-top" alt="Dish"/>
                <div className="card-body">
                    <h5 className="card-title">{name}</h5>
                    <p className="card-text">Price: ${price}</p>
                    <button onClick={editDish} className="btn btn-primary">Edit</button>
                    <button onClick={removeDish} className="btn btn-primary">Remove</button>

                </div>
            </div>
        </div>
    );
};

export default MenuItem;