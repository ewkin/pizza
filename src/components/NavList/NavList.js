import React from 'react';
import './NavList.css';
import NavItem from './NavItem/NavItem';

const NavList = () => {
    return (
        <ul className="NavigationItems">
            <NavItem to="/" exact>Home</NavItem>
            <NavItem to="/orders" exact>Orders</NavItem>
        </ul>
    );
};

export default NavList;