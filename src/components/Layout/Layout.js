import React from 'react';
import './Layout.css';
import NavList from "../NavList/NavList";
import logo from '../../assets/images/logo.png'
const Layout = props => {
    return (
        <>
            <header className="Toolbar">
                <div className="Toolbar-logo">
                    <div className="Logo">
                        <img src={logo} alt="logo"/>
                    </div>
                </div>
                <nav>
                    <NavList/>
                </nav>
            </header>
            <main className="Layout-Content">
                {props.children}
            </main>
        </>
    );
};

export default Layout;