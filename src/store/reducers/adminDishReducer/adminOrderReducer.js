import {ORDER_REQUEST, ORDER_SUCCESS} from "../../actions/adminOrderAction";

const initialState = {
    order: [],
    loading:true,

};

export const adminOrderReducer = (state = initialState, action) => {
    switch (action.type) {
        case ORDER_SUCCESS:
            return {...state, order: action.order, loading: false};
        case ORDER_REQUEST:
            return {...state, loading: true};
        default:
            return state;
    }
};