import {
    INIT_MENU,
    MENU_FAILURE,
    MENU_REQUEST,
    MENU_SUCCESS,
    SET_EDIT,
    SET_EDIT_DISH
} from "../../actions/adminDishAction";

const initialState = {
    menu: [],
    fetchError: false,
    edit:false,
    loading:true,
    id:undefined,
};

export const adminDishReducer = (state = initialState, action) => {
    switch (action.type) {
        case INIT_MENU:
            return {...state, menu: action.initMenu};
        case MENU_REQUEST:
            return {...state, loading: true};
        case MENU_SUCCESS:
            return {...state, edit: false, loading: false, id: undefined};
        case SET_EDIT:
                return {...state, edit: action.edit};
        case SET_EDIT_DISH:
            return  {...state, id: action.id};
        case MENU_FAILURE:
            return {...state, fetchError: action.error, edit: true};

        default:
            return state;
    }
};