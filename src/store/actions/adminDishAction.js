import {axiosPizza} from "../../axios-pizza";

export const ADD_DISH = 'ADD_DISH';
export const REMOVE_DISH = 'REMOVE_DISH';
export const INIT_MENU = 'INIT_MENU';

export const MENU_REQUEST = 'MENU_REQUEST';
export const MENU_SUCCESS = 'MENU_SUCCESS';
export const MENU_FAILURE = 'MENU_FAILURE';
export const SET_EDIT = 'SET_EDIT';
export const SET_EDIT_DISH = 'SET_EDIT_DISH';

export const menuRequest = () => ({type: MENU_REQUEST});
export const menuSuccess = () => ({type: MENU_SUCCESS});
export const menuFailure = error => ({type: MENU_FAILURE, error});
export const setEdit = edit => ({type: SET_EDIT, edit});
export const addDish = dish => ({type: ADD_DISH, dish});
export const removeDish = id => ({type: REMOVE_DISH, id});
export const initMenu = initMenu => ({type: INIT_MENU, initMenu});
export const setEditDish = id =>({type: SET_EDIT_DISH, id});

export const createMenuItem = menuItem => {
    return async dispatch => {
        try {
            await axiosPizza.post('/menu.json', menuItem);
            dispatch(menuSuccess());
        } catch (e) {
            dispatch(menuFailure(e));
        }
    };
};

export const editMenuItem = (menuItem, id) => {
    console.log(id);
    return async dispatch => {
        try {
            await axiosPizza.put('/menu/'+id+'.json', menuItem);
            dispatch(menuSuccess());
        } catch (e) {
            dispatch(menuFailure(e));
        }
    };
};

export const deleteMenuItem = id => {
    return async dispatch => {
        try {
            dispatch(menuRequest());
            await axiosPizza.delete('/menu/' + id + '.json');
            dispatch(menuSuccess());
        } catch (e) {
            console.error(e);
            dispatch(menuFailure(e));
        }
    };
};


export const fetchMenu = () => {
    return async dispatch => {
        try {
            const response = await axiosPizza.get('/menu.json');
            const menu = Object.keys(response.data).map(id => ({
                ...response.data[id], id
            }))
            dispatch(initMenu(menu));
        } catch (error) {
            dispatch(menuFailure(error));
        }
    };
};