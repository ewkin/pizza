import {axiosPizza} from "../../axios-pizza";


export const ORDER_REQUEST = 'ORDER_REQUEST';
export const ORDER_SUCCESS = 'ORDER_SUCCESS';
export const ORDER_FAILURE = 'ORDER_FAILURE';

export const orderRequest = () => ({type: ORDER_REQUEST});
export const orderSuccess = order => ({type: ORDER_SUCCESS, order});
export const orderFailure = error => ({type: ORDER_FAILURE, error});

export const fetchOrder = () => {
    return async dispatch => {
        try {
            const response = await axiosPizza.get('/order.json');
            const order = Object.keys(response.data).map(id => ({
                ...response.data[id], id
            }))
            dispatch(orderSuccess(order));
        } catch (error) {
            dispatch(orderFailure(error));
        }
    };
};

export const completeOrder = id => {
    return async dispatch => {
        try {
            dispatch(orderRequest());
            await axiosPizza.delete('/order/' + id + '.json');
        } catch (e) {
            console.error(e);
            dispatch(orderFailure(e));
        }
    };
};