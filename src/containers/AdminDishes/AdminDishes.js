import React, {useEffect} from 'react';
import AddForm from "../../components/AddForm/AddForm";
import Modal from "../../components/UI/Modal/Modal";
import {useDispatch, useSelector} from "react-redux";
import {deleteMenuItem, fetchMenu, setEdit, setEditDish} from "../../store/actions/adminDishAction";
import MenuItem from "../../components/MenuItem/MenuItem";

const AdminDishes = () => {
    const dispatch = useDispatch();
    const isEdit = useSelector(state => state.menu.edit);
    const menu = useSelector(state => state.menu.menu);
    const loading = useSelector(state => state.menu.loading);

    useEffect(() => {
        dispatch(fetchMenu());
    }, [dispatch, isEdit, loading]);


    const editCancelHandler = () => {
        dispatch(setEdit(false));
        dispatch(setEditDish(undefined));
    };

    const addToMenu = () => {
        dispatch(setEdit(true));
    };
    let idDish = undefined;

    const editPizza = id => {
        dispatch(setEditDish(id));
        dispatch(setEdit(true));
    }

    return (
        <>
            <Modal show={isEdit}
                   closed={editCancelHandler}
            >
                <AddForm
                    id={idDish}/>
            </Modal>
            <button onClick={addToMenu} className="btn btn-primary">Add New Dish</button>
            <div className="row">
                <div className="col-sm-12">
                    <div className="row">
                        {menu.map(menuItem => (
                            <MenuItem
                                key={menuItem.id}
                                name={menuItem.name}
                                price={menuItem.price}
                                url={menuItem.url}
                                removeDish={() => dispatch(deleteMenuItem(menuItem.id))}
                                editDish={() => editPizza(menuItem.id)}
                            />
                        ))}
                    </div>
                </div>
            </div>
        </>
    );
};

export default AdminDishes;