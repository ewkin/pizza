import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {completeOrder, fetchOrder} from "../../store/actions/adminOrderAction";

const AdminOrders = () => {
    const dispatch = useDispatch();
    const order = useSelector(state => state.order.order);
    const loading = useSelector(state => state.order.loading);

    useEffect(() => {
        dispatch(fetchOrder());
    }, [dispatch, loading]);

    let price = 0;


    return (
        <div>
            {order.map(order => (
                Object.keys(order).map(key => {
                    if (key !== 'id') {
                        price = price + order[key];
                        return <div key={order.id + price}>{key}: {order[key]}</div>
                    }
                    return <>
                        <div key={order.id}>Total price: {price}</div>
                        <button onClick={()=>dispatch(completeOrder(order.id))}  className="btn btn-primary">Complete Order</button>

                    </>
                })
            ))}
        </div>
    );
};

export default AdminOrders;