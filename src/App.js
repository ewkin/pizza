import React from 'react';
import {Route, Switch} from "react-router-dom";
import AdminDishes from "./containers/AdminDishes/AdminDishes";
import AdminOrders from "./containers/AdminOrders/AdminOrders";
import Layout from "./components/Layout/Layout";

const App = () => {
    return (
        <Layout>
            <Switch>
                <Route path="/" exact component={AdminDishes}/>
                <Route path="/orders" component={AdminOrders}/>
            </Switch>
        </Layout>
    );
};

export default App;